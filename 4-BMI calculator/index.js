
document.getElementById("calculate").addEventListener("click",calculateBmi);


function calculateBmi(){

    let height = document.getElementById("height").value;
    let weight = document.getElementById("weight").value;
    height = height/100;
    let bmi=weight/(height*height);
    document.getElementById("bmiValue").textContent = bmi.toFixed(2);
    let underWeight =  document.getElementById("Underweight");
    let normal =  document.getElementById("normal");
    let Overweight =  document.getElementById("Overweight");
    let obese =  document.getElementById("obese");
    let MorbidlyObese =  document.getElementById("MorbidlyObese");
    underWeight.classList.remove("bg-danger");
    normal.classList.remove("bg-danger");
    Overweight.classList.remove("bg-danger");
    obese.classList.remove("bg-danger");
    MorbidlyObese.classList.remove("bg-danger");

    
    if(bmi < 18.50){

        underWeight.classList.add("bg-danger");
    }
    else if(bmi > 18.50 && bmi< 24.9){
        normal.classList.add("bg-danger");
    }
    else if(bmi >24.9 && bmi< 29.9){
        Overweight.classList.add("bg-danger");
    }
    else if(bmi >29.9 && bmi< 39.9){
        obese.classList.add("bg-danger");
    }
    else if(bmi >39.9){
        MorbidlyObese.classList.add("bg-danger");
    }

      
   
}