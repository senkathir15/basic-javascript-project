let todoList = [];
document.getElementById("add").addEventListener("click", addtodo);

function addtodo() {
  const addOrEdit = document.getElementById("add").value;
  if (addOrEdit === "Add") {
    let value = document.getElementById("task").value;
    if (value !== "") {

      todoList.push({ id: todoList.length, do: value });
      
    } else {
      alert("Please Enter the value");
    }
  } else {
    const index = document.getElementById("index").value;
    let editvalue = document.getElementById("task").value;
    todoList[index].do = editvalue;
    document.getElementById("add").value = "Add";
    document.getElementById("index").value = "";
    document.getElementById("task").value = "";
  }
  showTodo();
}
function showTodo() {
  let task = "";
  todoList.forEach((todos, index) => {
    task += `<div class="d-flex   m-2  list-container">
        <p class="d-inline mr-auto">${todos.do}</p>
        <input type="button" class="btn m-2" id="edit" value="edit" onclick="editTask(${index},event);" >
        <input type="button" class="btn m-2" id="weight" value="Delete" onclick="remove(${index},event);" >
      </div>`;
  });
  console.log(task);
  document.getElementById("list").innerHTML = task;
}
function remove(index, event) {
  todoList.splice(index, 1);
  showTodo();
  event.preventDefault();
}

function editTask(index, event) {
  document.getElementById("task").value = todoList[index].do;
  document.getElementById("add").value = "edit";
  document.getElementById("index").value = index;
  event.preventDefault();
}



