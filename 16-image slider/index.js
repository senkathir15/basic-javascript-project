var prev = document.querySelector('.prev');
var next = document.querySelector('.next');
var index = 0;
var list = document.querySelectorAll('.slide');
var count = list.length;

prev.addEventListener('click', function() {
    list[index].classList.remove('visible');
    index--;
    if (index < 0){
        index = count-1;
        list[index].classList.add('visible');
    }
    list[index].classList.add('visible');
});

next.addEventListener('click', function() {
    list[index].classList.remove('visible');
    index++;
    if (index > count-1){
        index = 0;
    }
    list[index].classList.add('visible');
});